-- SUMMARY --

The Column Layout module allows you to create columnized content in a node's
body field or wherever an input format can be applied.

How to Use: You wrap a piece of content in BBCode like tags, the input filter
creates an evenly distributed set of columns using jQuery Columnizer or, if you
decide to use "manual" mode, a set of floated divs according to the provided
widths in percentages. If CSS3 Multicolumns and Flex-Box will ever see broad
browser support, the module's output will be adjusted accordingly. The module
also provides options for inserting manual column breaks, either by inserting
a tag ("manual break") or defining an HTML element ("auto break").

Example:

  [columns:auto:3]
  .. content ..
  [/columns]

=> Distributes the content evenly across 3 columns using jQuery Columnizer.

  [columns:manual:25:75]
  .. content in first column ..
  [columns:manual-break]
  .. content in second column..
  [/columns]

=> Creates 2 columns, 25% and 75% width. Column breaks must be inserted
manually.

  [columns:auto-break:h2:4]
  <h2>Heading</h2>
  .. content ..
  <h2>Heading</h2>
  .. content ..
  <h2>Heading</h2>
  .. content ..
  <h2>Heading</h2>
  .. content ..
  [/columns]

=> Distributes the content evenly across 4 columns, making sure that the column
break happens before HTML elements "<h2>".


Hints:
* Nesting multiple column sets isn't supported. Drupal already generates lots of
HTML markup by default, don't add to the mess by nesting column sets in the body
field.
* Automatic column break by HTML element currently only works for "auto" mode.
All manual column sets need manual column breaks.


-- REQUIREMENTS --

None.

(jQuery Columnizer is included.)


-- INSTALLATION --

* Install as usual, see
        http://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

* Enable the filter for one or more text formats at
        http://example.com/admin/config/content/formats
  (replacing example.com with your site name).  Follow the "configure" link for
  the format you want to use.  If you use the HTML filter after this one, make
  sure that it allows the tags that this filter produces.

-- INSTALLED HELP --

* After installation, you can see general instructions at
        http://example.com/admin/help/columns_layout
  and (if the filter has been added to any text format) usage information at
        http://example.com/filter/tips


-- CUSTOMIZATION --

None (use CSS to style to your liking).


-- CONTACT --

Current maintainer:
* Oliver Studer (genox) - http://drupal.org/user/530284
