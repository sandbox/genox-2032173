/**
 * @file
 * Column Layout - an input filter for Drupal 7.
 */

(function($){
  Drupal.behaviors.columns_layout = {
    attach: function (context, settings) {
      $('.column-layout.columnizer').each(function(index, value) {
        
        var manual_breaks = false;
        
        $(this).find('table, thead, tbody, tfoot, colgroup, caption, label, legend, script, style, textarea, button, object, embed, tr, th, td, li, h1, h2, h3, h4, h5, h6, form').addClass('dontsplit');
        $(this).find('h1, h2, h3, h4, h5, h6').addClass('dontend');
        $(this).find('br').addClass('removeiflast').addClass('removeiffirst');

        if($(this).attr('data-autobreak') == 1 && $(this).attr('data-autobreak-element') !== "") {
          $(this).find($(this).attr('data-autobreak-element')).prev().addClass('columnbreak');
          manual_breaks = true;
        }
        if( $('.column').css('float') !== 'none') {
        $(this).columnize({
          columns: $(this).attr('data-columns'),
          ignoreImageLoading: false,
          lastNeverTallest: false,
          buildOnce: false,
          manualBreaks: manual_breaks,
        });
        }
      });
    }
  };
})(jQuery);
